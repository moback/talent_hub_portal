# kfhub_internal_app

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 1.7.4.

# Architecture overview

This is a standard angular CLI project, which is using recommended
angular practices, but some additional things are included:

- airbnb ts-lint rule set added
- kfLib_lib for components and services, it has a npm dependency, but
  assets (images, less variables and translations) are included in
  this repo
- every static text string is coming from a translation file located
  inside `assets/languages` (`ngx-translate` is used)

The application is split into individual angular modules. Each modules
routes and components are placed into its own module directory, but
services are placed into `shared-sevices` module. Every module can
import `SharedServicesModule` safely assuming that all included
services are singletons. The top level module handles login stuff (via
using kflib) and navigation between modules. So adding a new
application module requires creating an angular module and adding a
link into `app-routing.module.ts`.

Here is an angular modules layout overview:

```
+----------------------+
|                      |        +-------------------------+
|  App module          |        |                         |
|                      +--------+     Shared services     |
|  * login             |        |                         |
|  * global routes     |        +-------------------------+
|  * notifications     |
|                      |
+------+----+----+-----+
       ^    ^    ^
       |    |    |
       |    |    +------------------------------------------------+
       |    |                                                     |
       |    +--------------------------+                          |
       |                               |                          |
+------+-----------------+      +------+---------------+    +-----+-----+
|                        |      |                      |    |           |
|  Work Day Integration  |      |  Pay Hub Management  |    |  New app  |
|     (competencies)     |      |       (payhub)       |    |           |
|                        |      |                      |    |           |
+------------------------+      +----------------------+    +-----------+
```

# Development setup

For this project you need to build a local dependency (`dist.tgz`). To do this you need to clone [kfhub_lib](https://bitbucket.org/arcadiakornferry/kfhub_lib/src/develop/) (NOTE: use `develop-v2` branch). Then run the following commands inside `kfhub_lib` directory:

- npm install
- npm run build
- npm run packagr

After just copy `dist.tgz` file into this directory and run `yarn install && npm start`.

NOTE: When `kfhub_lib` version is updated new assets should be copied into this repo.

# Test/prod setup

Make sure that `Development setup` is working, then run (add `-prod` flag if building for production):
```
ng build
```

__IMPORTANT:__ After that navigate to `dist` and create a new directory `app`, then put `assets` inside `app`.

To make sure than everything is working, run the following command to install a local server:
```
npm install http-server -g
```

From the `README.md` (this file) directory run:
```
http-server dist
```

Now you should be able to access the application at `http://localhost:8080/`.

Upload `dist` folder to S3.

## Development server

Run `npm start` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `-prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).
