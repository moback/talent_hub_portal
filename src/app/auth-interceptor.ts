import { Injectable } from '@angular/core';
import { HttpInterceptor } from '@angular/common/http/src/interceptor';
import { Router } from '@angular/router';
import { KfAuthService, KfSharedConstantsService, KfPopupService } from 'kfhub_lib';
import { HttpRequest, HttpHandler, HttpEvent } from '@angular/common/http';
import { Observable, Subject } from 'rxjs';
import { TranslateService } from '@ngx-translate/core';
import { MessageService } from './shared-services/message.service';
import * as _ from 'lodash';

@Injectable()
export class AuthInterceptor implements HttpInterceptor {
    private authExpiredText: string;
    private errorText: string;
    private okText: string;

    constructor(
        private kfAuth: KfAuthService,
        private constService: KfSharedConstantsService,
        private translate: TranslateService,
        private router: Router,
        private messageService: MessageService,
    ) {
        Observable.zip(
            this.translate.get('authExpiredText'),
            this.translate.get('error'),
            this.translate.get('ok'),
        ).subscribe(([authExpiredText, errorText, okText]) => {
            this.authExpiredText = authExpiredText;
            this.errorText = errorText;
            this.okText = okText;
        });
    }

    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        if (this.isProtectedApiCall(req)) {
            if (this.kfAuth.isAuthenticated()) {
                return next.handle(req).catch((e) => {
                    if (e.status === 401) {
                        this.handleAuthError();
                    } else {
                        this.showError(e);
                        throw e;
                    }
                    return Observable.of(null);
                });
            } else {
                this.handleAuthError();
            }
        } else {
            return next.handle(req);
        }
    }

    private handleAuthError() {
        this.router.navigate(['login']);
        setTimeout(
            () => {
                this.messageService.notify('error', this.errorText, this.authExpiredText);
            },
            0,
        );
    }

    private showError(e) {
        const msg = _.get(e, 'error.responseMessage', 'Unknown error');
        setTimeout(
            () => {
                this.messageService.notify('error', this.errorText, msg);
            },
            0,
        );
    }

    private isProtectedApiCall(req: HttpRequest<any>) {
        const base = this.constService.getBaseApiUrl();
        const login = this.constService.getLoginUrl();
        return req.url.startsWith(base) && req.url !== login;
    }
}
