import { Injectable } from '@angular/core';
import { CanLoad, CanActivate, Router, RouterStateSnapshot, ActivatedRouteSnapshot } from '@angular/router';
import * as _ from 'lodash';
import { PermissionsService } from './shared-services/permissions.service';
import { Observable } from 'rxjs/Observable';
import { MessageService } from './shared-services/message.service';

@Injectable()
export class AppPermissionsGuard implements CanActivate {
    private readonly namesToRoutesMap = {
        WORKDAY_COMP: 'competencies',
        PAY_HUB_MANAGEMENT: 'payhub',
        USER_ADMINISTRATION: 'permissions',
    };

    private readonly routeToNamesMap = _(this.namesToRoutesMap).toPairs().map(_.reverse).fromPairs().value();

    constructor(
        private router: Router,
        private permissionsService: PermissionsService,
        private messageService: MessageService,
    ) { }

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> {
        const routeName = _.get(_.first(route.url), 'path');
        const appName = this.routeToNamesMap[routeName];
        return this.permissionsService.getOwnPermissions()
            .map((permissions) => {
                if (routeName && _.get(permissions, `applications.${appName}`)) {
                    return true;
                } else {
                    if (appName) {
                        this.messageService.notify('error', 'Error', `You have no permissions to access ${appName}`);
                    }
                    const accessibleApplication = _.findKey(permissions.applications, _.identity);
                    if (accessibleApplication) {
                        this.router.navigate([`/app/${this.namesToRoutesMap[accessibleApplication]}`]);
                    } else {
                        this.router.navigate(['/login']);
                    }
                    return false;
                }
            });
    }
}
