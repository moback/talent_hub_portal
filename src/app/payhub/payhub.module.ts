import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { TranslateModule } from '@ngx-translate/core';
import { SharedServicesModule } from '../shared-services/shared-services.module';
import { KfComponentsModule, KfSharedModule, KfPopupService } from 'kfhub_lib';
import { PayhubRoutingModule } from './payhub-routing.module';
import { FormsModule } from '@angular/forms';
import { ListboxModule, ConfirmDialogModule } from 'primeng/primeng';
import { TableModule } from 'primeng/table';
import { ReportsComponent } from './reports/reports.component';
import { ConfirmationService } from 'primeng/components/common/confirmationservice';

@NgModule({
    imports: [
        CommonModule,
        TranslateModule.forChild(),
        FormsModule,
        SharedServicesModule,
        PayhubRoutingModule,
        KfComponentsModule,
        KfSharedModule,
        ListboxModule,
        TableModule,
        ConfirmDialogModule,
    ],
    declarations: [
        ReportsComponent,
    ],
    providers: [
        ConfirmationService,
    ],
})
export class PayhubModule { }
