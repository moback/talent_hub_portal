import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute, ActivatedRouteSnapshot } from '@angular/router';
import { Subject } from 'rxjs/Subject';
import { untilDestroyed } from 'ngx-take-until-destroy/dist/take-until-destroy';
import { PayhubService, ReportDataEntry } from '../../shared-services/payhub.service';
import { KfPopupService } from 'kfhub_lib';
import { Observable } from 'rxjs/Observable';
import { forkJoin } from 'rxjs/observable/forkJoin';
import 'rxjs/add/operator/ignoreElements';
import { ConfirmationService } from 'primeng/components/common/confirmationservice';
import { FileUploadService } from '../../shared-services/file-upload.service';

import * as _ from 'lodash';
import { ReportDataUploaderService } from '../../shared-services/report-data-uploader.service';

type fileType = 'image' | 'report';

@Component({
    selector: 'kf-hubint-reports',
    templateUrl: './reports.component.html',
    styleUrls: ['./reports.component.less'],
})
export class ReportsComponent implements OnInit, OnDestroy {
    private snapshot: ActivatedRouteSnapshot;
    private changes$ = new Subject<[string, string]>();
    clients: { id: string, name: string };
    countries: { id: string, name: string };
    availableReports: { type: string, name: string }[];
    reports: ReportDataEntry[];

    selectedClient: any;
    selectedCountry: any;

    constructor(
        private activatedRoute: ActivatedRoute,
        private router: Router,
        private payhubService: PayhubService,
        private popup: ConfirmationService,
        private fileUpload: ReportDataUploaderService,
    ) {
        this.snapshot = activatedRoute.snapshot;
    }

    ngOnInit() {
        const { data } = this.snapshot.data;
        this.clients = data.clients;
        this.countries = data.countries;
        this.availableReports = data.reportTypes;

        this.changes$.switchMap(
            ([clientId, countryCode]) => this.payhubService.getReportData(clientId, countryCode),
        ).pipe(
            untilDestroyed(this),
        ).subscribe((reports) => {
            const mapping = _.fromPairs(_.map(reports, r => [r.type, r]));
            this.reports = _.map(this.availableReports, r => _.assign({}, r, mapping[r.type]));
        });
    }

    update() {
        if (this.selectedClient != null && this.selectedCountry != null) {
            this.reports = null;
            this.changes$.next([this.selectedClient.id, this.selectedCountry.code]);
        }
    }

    reupload(ev) {
        this.popup.confirm({
            message: '',
            accept: () => {
                this.upload(ev);
            },
        });
    }

    upload(ev) {
        const { files } = ev.target;
        if (_.size(files) < 0) {
            return;
        }
        this.reports = null;
        const reportType = ev.target.attributes.reporttype.value;
        const resourceType = ev.target.attributes.resourcetype.value;
        this.fileUpload.upload(this.selectedClient.id, this.selectedCountry.id, reportType, resourceType, files[0])
            .pipe(untilDestroyed(this))
            .subscribe(() => this.update());
    }

    ngOnDestroy() {
    }
}
