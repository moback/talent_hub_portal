import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { PayhubDataResolver } from './payhub-data.resolver';
import { SharedServicesModule } from '../shared-services/shared-services.module';
import { ReportsComponent } from './reports/reports.component';

export const routes: Routes = [
    {
        path: 'report',
        resolve: {
            data: PayhubDataResolver,
        },
        component: ReportsComponent,
    },
    { path: '', redirectTo: 'report', pathMatch: 'full' },
];

@NgModule({
    imports: [
        CommonModule,
        RouterModule.forChild(routes),
        SharedServicesModule,
    ],
    exports: [
        RouterModule,
    ],
    providers: [
        PayhubDataResolver,
    ],
    declarations: [],
})
export class PayhubRoutingModule { }
