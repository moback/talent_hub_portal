import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { PayhubService } from '../shared-services/payhub.service';
import { Observable } from 'rxjs/Observable';
import { Injectable } from '@angular/core';

@Injectable()
export class PayhubDataResolver implements Resolve<any> {
    constructor(private payhubService: PayhubService) { }

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> {
        return this.payhubService.getPayReferenceData();
    }
}
