import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute, ActivatedRouteSnapshot } from '@angular/router';
import { Observable, Subject } from 'rxjs';
import { Permissions, PermissionsService } from '../../shared-services/permissions.service';
import { catchError } from 'rxjs/operators/catchError';
import * as _ from 'lodash';
import { untilDestroyed } from 'ngx-take-until-destroy/dist/take-until-destroy';
import { MessageService } from '../../shared-services/message.service';
import { TranslateService } from '@ngx-translate/core';

@Component({
    selector: 'kf-hubint-user-details',
    templateUrl: './user-details.component.html',
    styleUrls: ['./user-details.component.less'],
})
export class UserDetailsComponent implements OnInit, OnDestroy {
    private snapshot: ActivatedRouteSnapshot;
    private updateSubject$ = new Subject<number>();
    private successText = '';
    private permissionsUpdatedText = '';

    public user: any;
    public loading = true;
    public error = null;
    public permissions: Permissions = { applications: {} };

    constructor(
        private activatedRoute: ActivatedRoute,
        private router: Router,
        private permissionsService: PermissionsService,
        private messageService: MessageService,
        private translate: TranslateService,
    ) {
        this.snapshot = activatedRoute.snapshot;
    }

    ngOnInit() {
        this.user = this.snapshot.data.user.user;

        Observable.zip(
            this.translate.get('success'),
            this.translate.get('permissionsUpdatedText'),
        ).pipe(
            untilDestroyed(this),
        ).subscribe(([successText, permissionsUpdatedText]) => {
            this.successText = successText;
            this.permissionsUpdatedText = permissionsUpdatedText;
        });

        this.updateSubject$.switchMap(
            id => this.permissionsService.getPermissions(id),
        ).pipe(
            catchError((e) => {
                this.error = e;
                return Observable.of(null);
            }),
            untilDestroyed(this),
        ).subscribe((permissions?: Permissions) => {
            this.loading = false;
            if (permissions) {
                this.error = null;
                this.permissions = _.cloneDeep(permissions);
            }
        });

        this.refresh();
    }

    back() {
        this.router.navigate(['../../list'], { relativeTo: this.activatedRoute });
    }

    update() {
        this.permissionsService.setPermissions(this.user.id, this.permissions)
            .pipe(untilDestroyed(this))
            .subscribe(() => {
                this.messageService.notify('success', this.successText, this.permissionsUpdatedText);
                this.refresh();
            });
    }

    private refresh() {
        if (this.user != null) {
            this.updateSubject$.next(this.user.id);
        } else {
            this.loading = false;
        }
    }

    ngOnDestroy() {
    }
}
