import { Component, OnInit, ViewChild } from '@angular/core';
import { UsersService } from '../../shared-services/users.service';
import { UsersMetadataService } from '../../shared-services/users-metadata.service';
import {
    KfImetaSearchFilterOutput,
    KfFilteredSearchResultsComponent,
} from 'kfhub_lib';

@Component({
    selector: 'kf-hubint-users-list',
    templateUrl: './users-list.component.html',
    styleUrls: ['./users-list.component.less'],
})
export class UsersListComponent implements OnInit {
    @ViewChild(KfFilteredSearchResultsComponent) searchResultsComponent: KfFilteredSearchResultsComponent;

    private readonly columnsMetadata = [
        {
            columnTitle: 'name',
            columnAlignment: 'left',
            columnData: this.getUserName.bind(this),
            columnHrefData: this.getPermissionsUrl.bind(this),
        },
        {
            columnTitle: 'EmailLabel',
            columnName: 'email',
            columnAlignment: 'left',
        },
        {
            columnTitle: 'role',
            columnName: 'roleName',
            columnAlignment: 'left',
        },
        {
            columnTitle: 'jobTitle',
            columnName: 'jobTitle',
            columnAlignment: 'left',
        },
        {
            columnTitle: 'location',
            columnName: 'locationDescription',
            columnAlignment: 'left',
        },
        {
            columnTitle: 'created',
            columnName: 'createdOn',
            columnAlignment: 'left',
        },
    ];

    constructor(public usersService: UsersService, public usersMetaService: UsersMetadataService) { }

    ngOnInit() {
    }

    searchFilterChange(ev: KfImetaSearchFilterOutput) {
        this.searchResultsComponent.searchString = ev.searchString;
        this.searchResultsComponent.appliedFilters = ev.appliedFilters;
        this.searchResultsComponent.refreshResults();
    }

    getPermissionsUrl(data) {
        return `../edit/${data.id}`;
    }

    getUserName(data) {
        return data.firstName + ' ' + data.lastName;
    }
}
