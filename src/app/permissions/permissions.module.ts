import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UsersListComponent } from './users-list/users-list.component';
import { UserDetailsComponent } from './user-details/user-details.component';
import { SharedServicesModule } from '../shared-services/shared-services.module';
import { PermissionsRoutingModule } from './permissions-routing.module';
import {
    KfComponentsModule,
    KfSharedModule,
} from 'kfhub_lib';
import { TranslateModule } from '@ngx-translate/core';
import { FormsModule } from '@angular/forms';

@NgModule({
    imports: [
        CommonModule,
        TranslateModule.forChild(),
        SharedServicesModule,
        PermissionsRoutingModule,
        KfComponentsModule,
        KfSharedModule,
        FormsModule,
    ],
    declarations: [UsersListComponent, UserDetailsComponent],
})
export class PermissionsModule { }
