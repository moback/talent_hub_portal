import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { UsersListComponent } from './users-list/users-list.component';
import { UserDetailsComponent } from './user-details/user-details.component';
import { SharedServicesModule } from '../shared-services/shared-services.module';
import { UserDetailsResolver } from './user-details.resolver';

export const routes: Routes = [
    { path: 'users/list', component: UsersListComponent },
    {
        path: 'users/edit/:id',
        component: UserDetailsComponent,
        resolve: {
            user: UserDetailsResolver,
        },
    },
    { path: '', redirectTo: 'users/list', pathMatch: 'full' },
];

@NgModule({
    imports: [
        CommonModule,
        RouterModule.forChild(routes),
        SharedServicesModule,
    ],
    exports: [
        RouterModule,
    ],
    providers: [
        UserDetailsResolver,
    ],
    declarations: [],
})
export class PermissionsRoutingModule { }
