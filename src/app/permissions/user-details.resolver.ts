import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { UsersService } from '../shared-services/users.service';
import { Observable } from 'rxjs/Observable';
import { Injectable } from '@angular/core';
import * as _ from 'lodash';

@Injectable()
export class UserDetailsResolver implements Resolve<any> {
    constructor(private usersService: UsersService) { }

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> {
        const id = route.paramMap.get('id');
        return this.usersService.getById(_.toInteger(id));
    }
}
