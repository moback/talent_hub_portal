import { Component, OnInit, OnDestroy } from '@angular/core';
import { Message } from 'primeng/components/common/message';
import { MessageService } from '../shared-services/message.service';
import { untilDestroyed } from 'ngx-take-until-destroy/dist/take-until-destroy';

@Component({
    selector: 'kf-hubint-notification',
    templateUrl: './notification.component.html',
    styleUrls: ['./notification.component.less'],
})
export class NotificationComponent implements OnInit, OnDestroy {
    msgs: Message[] = [];

    constructor(private messageService: MessageService) { }

    ngOnInit() {
        this.messageService.notificationChange
            .pipe(untilDestroyed(this))
            .subscribe((notifications) => {
                this.msgs = [];
                this.msgs.push(notifications);
            });
    }

    ngOnDestroy() {
    }
}
