import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { KfAuthGuardService } from 'kfhub_lib';
import { AppPermissionsGuard } from './app-permissions.guard';

export const routes: Routes = [
    { path: 'login', component: LoginComponent },
    {
        path: 'app',
        canActivate: [KfAuthGuardService],
        children: [
            {
                path: '',
                redirectTo: '', // authguard will take care of redirecting to an accessible app
                pathMatch: 'full',
                canActivate: [AppPermissionsGuard],
            },
            {
                path: 'competencies',
                loadChildren: './competencies/competencies.module#CompetenciesModule',
                canActivate: [AppPermissionsGuard],
            },
            {
                path: 'payhub',
                loadChildren: './payhub/payhub.module#PayhubModule',
                canActivate: [AppPermissionsGuard],
            },
            {
                path: 'permissions',
                loadChildren: './permissions/permissions.module#PermissionsModule',
                canActivate: [AppPermissionsGuard],
            },
        ],
    },
    { path: '**', redirectTo: 'app', pathMatch: 'full' },
];

@NgModule({
    imports: [
        CommonModule,
        RouterModule.forRoot(routes),
    ],
    exports: [
        RouterModule,
    ],
    providers: [
        AppPermissionsGuard,
    ],
    declarations: [],
})
export class AppRoutingModule { }
