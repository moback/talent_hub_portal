import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import {
    KfComponentsModule,
    KfSharedModule,
    KfIdleService,
    KfPopupService,
    KfAuthGuardService,
    KfAuthService,
    KfSharedConstantsService,
    environment,
} from 'kfhub_lib';
import { AppComponent } from './app.component';
import { HttpClientModule, HttpClient, HTTP_INTERCEPTORS } from '@angular/common/http';
import { TranslateModule, TranslateLoader, TranslateService } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { RouterModule } from '@angular/router';
import { SharedServicesModule } from './shared-services/shared-services.module';
import { LoginComponent } from './login/login.component';
import { NgIdleModule } from '@ng-idle/core/src/module';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { ModalModule } from 'ngx-modialog';
import { NavigationComponent } from './navigation/navigation.component';
import { SharedModule, SidebarModule, MenuModule, GrowlModule } from 'primeng/primeng';
import { AuthInterceptor } from './auth-interceptor';
import { AppRoutingModule } from './app-routing.module';
import { NotificationComponent } from './notification/notification.component';
import { NgProgressModule } from '@ngx-progressbar/core';
import { NgProgressHttpModule } from '@ngx-progressbar/http';
import { NgProgressRouterModule } from '@ngx-progressbar/router';

export function httpLoaderFactory(http: HttpClient) {
    const prefix = environment().appUrlPrefix;
    return new TranslateHttpLoader(http, `${prefix}/assets/languages/`, '.json');
}

@NgModule({
    declarations: [
        AppComponent,
        LoginComponent,
        NavigationComponent,
        NotificationComponent,
    ],
    imports: [
        BrowserModule,
        BrowserAnimationsModule,
        HttpClientModule,
        TranslateModule.forRoot({
            loader: {
                provide: TranslateLoader,
                useFactory: httpLoaderFactory,
                deps: [HttpClient],
            },
        }),
        NgbModule.forRoot(),
        ModalModule.forRoot(),
        NgIdleModule.forRoot(),
        AppRoutingModule,
        SharedServicesModule.forRoot(),
        SharedModule,
        SidebarModule,
        MenuModule,
        GrowlModule,
        KfSharedModule,
        KfComponentsModule,
        NgProgressModule.forRoot(),
        NgProgressHttpModule,
        NgProgressRouterModule,
    ],
    providers: [
        KfIdleService,
        KfPopupService,
        KfAuthGuardService,
        {
            provide: HTTP_INTERCEPTORS,
            useClass: AuthInterceptor,
            multi: true,
        },
    ],
    bootstrap: [AppComponent],
})
export class AppModule {
    constructor(private translate: TranslateService) {
        translate.addLangs(['en', 'de', 'es-ar', 'ja', 'pl', 'tr', 'zh']);
        translate.setDefaultLang('en');
        translate.use('en');
    }
}
