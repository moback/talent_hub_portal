import { Component, OnInit, OnDestroy } from '@angular/core';
import { KfAuthService } from 'kfhub_lib';
import { untilDestroyed } from 'ngx-take-until-destroy';
import { Router, ActivatedRoute, ActivatedRouteSnapshot, ResolveEnd, GuardsCheckEnd } from '@angular/router';
import { MenuItem } from 'primeng/api';
import { TranslateService } from '@ngx-translate/core';
import { Observable } from 'rxjs';
import { PermissionsService } from '../shared-services/permissions.service';

@Component({
    selector: 'kf-hubint-navigation',
    templateUrl: './navigation.component.html',
    styleUrls: ['./navigation.component.less'],
})
export class NavigationComponent implements OnInit, OnDestroy {
    public isMenuVisible = false;
    public menuItems: MenuItem[];

    constructor(
        private router: Router,
        private authService: KfAuthService,
        private translate: TranslateService,
        private permissionsService: PermissionsService,
    ) { }

    public logout() {
        this.authService.removeSessionInfo()
            .pipe(untilDestroyed(this))
            .subscribe(() => (
                this.router.navigate(['login'])
            ));
    }

    public showMenu() {
        this.isMenuVisible = true;
    }

    public hideMenu() {
        this.isMenuVisible = false;
    }

    ngOnInit() {
        Observable.zip(
            this.translate.get('applications'),
            this.translate.get('workDayIntApp'),
            this.translate.get('payHubManageApp'),
            this.translate.get('permsManageApp'),
            this.permissionsService.getOwnPermissions(),
        ).pipe(
            untilDestroyed(this),
        ).subscribe(([label, wd, ph, pm, permissions]) => {
            const items = [];
            if (permissions.applications) {
                if (permissions.applications.WORKDAY_COMP) {
                    items.push({
                        label: wd,
                        routerLink: '/app/competencies',
                        command: () => this.hideMenu(),
                    });
                }
                if (permissions.applications.PAY_HUB_MANAGEMENT) {
                    items.push({
                        label: ph,
                        routerLink: '/app/payhub',
                        command: () => this.hideMenu(),
                    });
                }
                if (permissions.applications.USER_ADMINISTRATION) {
                    items.push({
                        label: pm,
                        routerLink: '/app/permissions',
                        command: () => this.hideMenu(),
                    });
                }
            }
            this.menuItems = [{
                label,
                items,
            }];
        });
    }

    ngOnDestroy() { }
}
