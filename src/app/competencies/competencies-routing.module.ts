import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { ClientsListComponent } from './clients-list/clients-list.component';
import { ClientDetailsComponent } from './client-details/client-details.component';

export const routes: Routes = [
    { path: 'clients/list', component: ClientsListComponent },
    { path: 'clients/details/:id', component: ClientDetailsComponent },
    { path: 'clients/add', component: ClientDetailsComponent },
    { path: '', redirectTo: 'clients/list', pathMatch: 'full' },
];

@NgModule({
    imports: [
        CommonModule,
        RouterModule.forChild(routes),
    ],
    exports: [
        RouterModule,
    ],
    declarations: [],
})
export class CompetenciesRoutingModule { }
