import { Component, OnInit, ViewChild } from '@angular/core';
import { ClientsService } from '../../shared-services/clients.service';
import { untilDestroyed } from 'ngx-take-until-destroy';
import { OnDestroy } from '@angular/core/src/metadata/lifecycle_hooks';
import { Subject } from 'rxjs/Subject';
import { KfFilteredSearchResultsComponent } from 'kfhub_lib';
import { Router, ActivatedRoute } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { Observable } from 'rxjs';

@Component({
    selector: 'kf-hubint-clients-list',
    templateUrl: './clients-list.component.html',
    styleUrls: ['./clients-list.component.css'],
})
export class ClientsListComponent implements OnInit, OnDestroy {
    private inputSubject$ = new Subject<string>();
    private data = [];
    public clientName: string;

    @ViewChild(KfFilteredSearchResultsComponent) searchResultsComponent: KfFilteredSearchResultsComponent;

    public columnsMetadata = [
        {
            columnTitle: 'clientId',
            columnName: 'clientId',
            columnAlignment: 'left',
            columnHrefData: this.getDetailsUrl.bind(this),
        },
        {
            columnTitle: 'sapClientId',
            columnName: 'sapClientId',
            columnAlignment: 'left',
        },
        {
            columnTitle: 'clientName',
            columnName: 'clientName',
            columnAlignment: 'left',
        },
    ];

    constructor(
        public clientsService: ClientsService,
        private router: Router,
        private activatedRoute: ActivatedRoute,
        private translate: TranslateService,
    ) { }

    ngOnInit() {
        this.inputSubject$
            .debounceTime(500)
            .pipe(untilDestroyed(this))
            .subscribe((clientName: string) => {
                this.searchResultsComponent.searchString = clientName;
                this.searchResultsComponent.appliedFilters = [];
                this.searchResultsComponent.refreshResults();
            });
    }

    inputChange(data) {
        this.inputSubject$.next(data);
    }

    getDetailsUrl(data) {
        return `../details/${data.clientId}`;
    }

    ngOnDestroy() {
        this.inputSubject$.complete();
    }

    createClient() {
        this.router.navigate([`../add/`], { relativeTo: this.activatedRoute });
    }
}
