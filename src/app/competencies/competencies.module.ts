import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ClientsListComponent } from './clients-list/clients-list.component';
import { ClientDetailsComponent } from './client-details/client-details.component';
import { SharedServicesModule } from '../shared-services/shared-services.module';
import { RouterModule } from '@angular/router';
import {
    KfComponentsModule,
    KfSharedModule,
} from 'kfhub_lib';
import { FormsModule } from '@angular/forms';
import { TranslateModule } from '@ngx-translate/core';
import { CompetenciesRoutingModule } from './competencies-routing.module';
import { RadioButtonModule } from 'primeng/radiobutton';

@NgModule({
    imports: [
        CommonModule,
        TranslateModule.forChild(),
        SharedServicesModule,
        CompetenciesRoutingModule,
        KfComponentsModule,
        KfSharedModule,
        FormsModule,
        RadioButtonModule,
    ],
    declarations: [ClientsListComponent, ClientDetailsComponent],
})
export class CompetenciesModule { }
