import { Component, OnInit, OnDestroy } from '@angular/core';
import { untilDestroyed } from 'ngx-take-until-destroy';
import { ActivatedRouteSnapshot, ActivatedRoute, Router } from '@angular/router';
import * as _ from 'lodash';
import { ClientsService, ClientData, ClientDetails } from '../../shared-services/clients.service';
import { catchError } from 'rxjs/operators/catchError';
import { Observable, Subject } from 'rxjs';
import { MessageService } from '../../shared-services/message.service';
import { TranslateService } from '@ngx-translate/core';

@Component({
    selector: 'kf-hubint-client-details',
    templateUrl: './client-details.component.html',
    styleUrls: ['./client-details.component.css'],
})
export class ClientDetailsComponent implements OnInit, OnDestroy {
    private updateSubject$ = new Subject<number>();
    private snapshot?: ActivatedRouteSnapshot;
    private successText = '';
    private competenciesPublishedText = '';
    private clientCreatedText = '';
    private clientUpdatedText = '';

    public id: number;
    public error = null;
    public data: ClientDetails = {
        workdayDetails: {},
    };
    public clientForm = null;
    public loading = true;
    public changed = false;

    constructor(
        private activatedRoute: ActivatedRoute,
        private clientsService: ClientsService,
        private messageService: MessageService,
        private translate: TranslateService,
        private router: Router,
    ) {
        this.snapshot = activatedRoute.snapshot;
    }

    formEventsHandler(ev) {
        if (ev.type === 'change') {
            this.changed = true;
        }
    }

    ngOnInit() {
        Observable.zip(
            this.translate.get('success'),
            this.translate.get('competenciesPublished'),
            this.translate.get('clientCreated'),
            this.translate.get('clientUpdated'),
        ).pipe(
            untilDestroyed(this),
        ).subscribe(([successText, competenciesPublishedText, clientCreatedText, clientUpdatedText]) => {
            this.successText = successText;
            this.competenciesPublishedText = competenciesPublishedText;
            this.clientCreatedText = clientCreatedText;
            this.clientUpdatedText = clientUpdatedText;
        });

        this.updateSubject$.switchMap(
            clientId => this.clientsService.getClientInfo(clientId).pipe(catchError((e) => {
                this.error = e;
                return Observable.of(null);
            })),
        ).pipe(untilDestroyed(this)).subscribe((data: ClientData) => {
            this.loading = false;
            this.changed = false;
            if (data) {
                this.error = null;
                this.data = _.cloneDeep(data.clientInfo);
                _.defaults(this.data, { workdayDetails: {} });
            }
        });

        const { id } = this.snapshot.params;
        this.id = id;
        this.refresh();
    }

    private refresh() {
        if (this.id != null) {
            this.updateSubject$.next(this.id);
        } else {
            this.loading = false;
        }
    }

    isNew() {
        return this.id == null;
    }

    showUpdateControls() {
        return this.id != null && this.error == null && !this.loading;
    }

    publishCompetencies() {
        this.clientsService.publishCompetencies(this.id).pipe(untilDestroyed(this)).subscribe(() => {
            this.messageService.notify('success', this.successText, this.competenciesPublishedText);
            this.refresh();
        });
    }

    back() {
        this.router.navigate(['../../list'], { relativeTo: this.activatedRoute });
    }

    createClient() {
        this.clientsService.createClient(this.data).pipe(untilDestroyed(this)).subscribe((id) => {
            this.messageService.notify('success', this.successText, this.clientCreatedText);
            this.router.navigate([`../details/${id}`], { relativeTo: this.activatedRoute });
        });
    }

    updateClient() {
        const clientInfo = _.pick(this.data, ['clientId', 'workdayDetails']);
        this.clientsService.updateClient({ clientInfo }).pipe(untilDestroyed(this)).subscribe(() => {
            this.messageService.notify('success', this.successText, this.clientUpdatedText);
            this.refresh();
        });
    }

    ngOnDestroy() { }
}
