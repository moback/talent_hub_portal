import { NgModule, ModuleWithProviders } from '@angular/core';
import { ClientsService } from './clients.service';
import { MessageService } from './message.service';
import { PayhubService } from './payhub.service';
import { FileUploadService } from './file-upload.service';
import { ReportDataUploaderService } from './report-data-uploader.service';
import { UsersService } from './users.service';
import { UsersMetadataService } from './users-metadata.service';
import { FilterMetadataConverterService } from './filter-metadata-converter.service';
import { PermissionsService } from './permissions.service';

@NgModule({
    imports: [],
    declarations: [],
})
export class SharedServicesModule {
    static forRoot(): ModuleWithProviders {
        return {
            ngModule: SharedServicesModule,
            providers: [
                ClientsService,
                MessageService,
                PayhubService,
                FileUploadService,
                ReportDataUploaderService,
                UsersService,
                UsersMetadataService,
                PermissionsService,
                FilterMetadataConverterService,
            ],
        };
    }
}
