import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import * as _ from 'lodash';
import 'rxjs/add/operator/delay';

export type ApplicationNames = 'WORKDAY_COMP' | 'PAY_HUB_MANAGEMENT' | 'USER_ADMINISTRATION';

export interface Permissions {
    applications: {
        [name in ApplicationNames]?: boolean;
    };
}

export interface PermissionsPayload {
    applications: {
        name: ApplicationNames;
        access: boolean;
    }[];
}

@Injectable()
export class PermissionsService {
    constructor() { }

    public getOwnPermissions(): Observable<Permissions> {
        const ownPermissions = {
            applications: {
                WORKDAY_COMP: true,
                PAY_HUB_MANAGEMENT: true,
                USER_ADMINISTRATION: true,
            },
        };
        return Observable.of(ownPermissions).delay(500);
    }

    public getPermissions(userId: number): Observable<Permissions> {
        return Observable.of({
            applications: {
                WORKDAY_COMP: true,
                PAY_HUB_MANAGEMENT: false,
                USER_ADMINISTRATION: true,
            },
        }).delay(500);
    }

    public setPermissions(userId: number, permissions: Permissions): Observable<any> {
        const data = this.toServerPayload(permissions);
        console.dir(data);
        return Observable.of(true).delay(500);
    }

    private fromServerPayload(perms: PermissionsPayload): Permissions {
        if (perms == null) {
            return {
                applications: {},
            };
        }
        const applications = _(perms.applications).map(perm => [perm.name, perm.access]).fromPairs().value();
        return { applications };
    }

    private toServerPayload(perms: Permissions): PermissionsPayload {
        const applications = _(perms.applications)
            .toPairs()
            .map(([name, access]: [ApplicationNames, boolean]) => ({ name, access }))
            .value();
        return { applications };
    }
}
