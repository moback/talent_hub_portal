import { TestBed, inject } from '@angular/core/testing';

import { FilterMetadataConverterService } from './filter-metadata-converter.service';

describe('FilterMetadataConverterService', () => {
    beforeEach(() => {
        TestBed.configureTestingModule({
            providers: [FilterMetadataConverterService],
        });
    });

    it('should be created', inject([FilterMetadataConverterService], (service: FilterMetadataConverterService) => {
        expect(service).toBeTruthy();
    }));
});
