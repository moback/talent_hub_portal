import { TestBed, inject } from '@angular/core/testing';

import { ReportDataUploaderService } from './report-data-uploader.service';

describe('ReportDataUploaderService', () => {
    beforeEach(() => {
        TestBed.configureTestingModule({
            providers: [ReportDataUploaderService],
        });
    });

    it('should be created', inject([ReportDataUploaderService], (service: ReportDataUploaderService) => {
        expect(service).toBeTruthy();
    }));
});
