import { TestBed, inject } from '@angular/core/testing';

import { UsersMetadataService } from './users-metadata.service';

describe('UsersMetadataService', () => {
    beforeEach(() => {
        TestBed.configureTestingModule({
            providers: [UsersMetadataService],
        });
    });

    it('should be created', inject([UsersMetadataService], (service: UsersMetadataService) => {
        expect(service).toBeTruthy();
    }));
});
