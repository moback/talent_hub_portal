import { Injectable } from '@angular/core';
import { Subject } from 'rxjs/Subject';

type Severities = 'success' | 'info' | 'warn' | 'error';

@Injectable()
export class MessageService {
    private lastId = 0;
    notificationChange: Subject<Object> = new Subject<Object>();

    notify(severity: Severities, summary: string, detail: string) {
        this.lastId = this.lastId + 1;
        this.notificationChange.next({ id: this.lastId, severity, summary, detail });
    }
}
