import { TestBed, inject } from '@angular/core/testing';

import { PayhubService } from './payhub.service';

describe('PayhubService', () => {
    beforeEach(() => {
        TestBed.configureTestingModule({
            providers: [PayhubService],
        });
    });

    it('should be created', inject([PayhubService], (service: PayhubService) => {
        expect(service).toBeTruthy();
    }));
});
