import { Component, OnDestroy, OnInit } from '@angular/core';
import * as _ from 'lodash';
import { Router, NavigationEnd } from '@angular/router';
import { KfAuthService } from 'kfhub_lib';
import { Subject } from 'rxjs/Subject';
import { untilDestroyed } from 'ngx-take-until-destroy';

@Component({
    selector: 'kf-app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.css'],
})
export class AppComponent implements OnDestroy, OnInit {
    public showNavigation = false;
    public isBlocked = false;

    constructor(private router: Router) {
    }

    ngOnInit() {
        this.router.events
            .filter(event => event instanceof NavigationEnd)
            .pipe(untilDestroyed(this))
            .subscribe((event: NavigationEnd) => (
                this.showNavigation = !_.includes(event.url, '/login')
            ));
    }

    block() {
        this.isBlocked = true;
    }

    unblock() {
        this.isBlocked = false;
    }

    ngOnDestroy() { }
}
